CS 440 Final Project:
    Internet Speed Test/ Monitor

Group Members: Daniel Kelchner, Erick Pulido, Jose Untalan, Sam Sanchez, and Steven Truong

General Notes:
    
    1.
    As written, the script works under the assumption that it is being used within a specific environment.
    By this, we suggest that those files called upon in the script are being accessed by relative paths which should not be altered.
    In order to make the following code work on your machine, make sure to modifiy the included paths.

    2.
    Before starting, we need to ensure the raspberry pi is up-to-date. We can perform updates by using the following two commands:
    sudo apt-get update
    sudo apt-get upgrade

    We will be using a Python library called, "speedtest-cli."
    In order to access this library, however, we need to install a package called, "Python.pip."
    Run the following commands:
    sudo apt-get install python-pip
    sudo pip install speedtest-cli

    3.
    All files included in this repository have been sourced from external parties.
    speedtest.py is derived from, "https://pimylifeup.com/raspberry-pi-internet-speed-monitor/."
    dKelchnerIndProj.html is derived from, "https://www.youtube.com/watch?v=xGmXxpIj6vs."


